//
//  AppGeneral.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit

class AppGeneral: UIResponder {
    @objc var window: UIWindow?
    var coordinator: AppFlow?
    
    func createAndStartAppCoordinator() {
        window?.makeKeyAndVisible()
        let coordinator = createAppCoordinator()
        coordinator.start(with: .none, animated: false)
        self.coordinator = coordinator
    }
    
    private func createAppCoordinator() -> AppFlow {
        guard let window = self.window else {
            fatalError("Unreal case ;)")
        }
        let dependencies = AppFlowDependencies()
        let mainCoordinator = AppFlow(window: window,
                                             appDependencies: dependencies)
        return mainCoordinator
    }
}
