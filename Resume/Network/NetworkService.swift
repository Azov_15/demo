//
//  NetworkService.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import Moya
import RxSwift

protocol INetworkService {
    static var apiToken: String { get }
    static var baseUrl: URL { get }
    func getForecast(sity: String, days: UInt) -> Observable<[String: Any]>
}

final class NetworkService: INetworkService {
    
    private let provider = MoyaProvider<ForecastAPI>()
    
    static let apiToken = "4068818c0001468a9e762437212705"
    static let baseUrl = URL(string: "http://api.weatherapi.com/v1/")!
    
    func getForecast(sity: String, days: UInt) -> Observable<[String: Any]> {
        return provider.rx
            .request(.loadForecast(city: sity, days: days))
            .asObservable()
            .filterSuccessfulStatusCodes()
            .map { response in
                let json = (try? response.mapJSON()) as? [String: Any] ?? [:]
                return json
            }
    }
}

