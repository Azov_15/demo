//
//  ForecastRequests.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import Moya

enum ForecastAPI {
    case loadForecast(city: String, days: UInt)
}

extension ForecastAPI: TargetType {
    var baseURL: URL {
        NetworkService.baseUrl
    }
    
    var path: String {
        switch self {
        case .loadForecast:
            return "forecast.json"
        }
    }
    
    var method: Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .loadForecast(city: let city, days: let days):
            let params = ["key" : NetworkService.apiToken,
                          "q" : city,
                          "days" : String(days),
                          "aqi" : "no",
                          "alerts" : "no",
                          "lang": "ru"]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
}
