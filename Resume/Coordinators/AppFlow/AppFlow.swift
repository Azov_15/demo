//
//  AppFlow.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit
import RxSwift

enum EmptyOption {
    case none
}

enum DefaultOutput {
    case back
}

final class AppFlow: BaseCoordinator, Coordinatable {
    
    typealias InputType = EmptyOption
    typealias OutputType = EmptyOption
    
    var output: ((EmptyOption) -> Void)?
    
    private var window: UIWindow
    private let appDependencies: IAppFlowDependencies
    
    init(window: UIWindow, appDependencies: IAppFlowDependencies) {
        self.window = window
        window.overrideUserInterfaceStyle = .light
        self.appDependencies = appDependencies
        super.init()
    }
    
    func start(with option: EmptyOption, animated: Bool) {
        let myselfFlow = appDependencies.coordinatorFactory.makeMyselfRepresentationCoordinator(window: window)
        myselfFlow.start(with: .window(window), animated: true)
        myselfFlow.output = { [weak self, weak myselfFlow] output in
            guard let self = self,
                  let strongCoordinator = myselfFlow else {
                return
            }
            switch output {
            case .back:
                self.removeDependency(strongCoordinator)
                self.showMainTabBar()
            }
        }
        addDependency(myselfFlow)
    }
    
    private func showMainTabBar() {
        let tabBarFlow = appDependencies.coordinatorFactory.makeTabCoordinator(window: window,
                                                                               coordinatorFactory: appDependencies.coordinatorFactory)
        tabBarFlow.start(with: .none, animated: true)
        tabBarFlow.output = { [weak self, weak tabBarFlow] output in
            guard let self = self,
                  let strongCoordinator = tabBarFlow else {
                return
            }
            switch output {
            case .none:
                self.removeDependency(strongCoordinator)
            }
        }
        addDependency(tabBarFlow)
    }
}
