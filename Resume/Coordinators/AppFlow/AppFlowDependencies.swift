//
//  AppFlowDependencies.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import Foundation

protocol IAppFlowDependencies {
    var coordinatorFactory: ICoordinatorFactory { get }
}

final class AppFlowDependencies: IAppFlowDependencies {
    var coordinatorFactory: ICoordinatorFactory
    
    init() {
        coordinatorFactory = CoordinatorFactory()
    }
}
