//
//  MyselfRepresentationFlow.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit

enum MyselfRepresentationInput {
    case window(UIWindow)
}

protocol MyselfRepresentationFlowDelegate: AnyObject {
    func nextPressed()
    func gotoTestApp()
}

final class MyselfRepresentationFlow: BaseCoordinator, Coordinatable {
    
    typealias InputType = MyselfRepresentationInput
    typealias OutputType = DefaultOutput
    
    var output: ((DefaultOutput) -> Void)?
    
    private var transition = MineFaceVCTransition()
    
    override init() {
        super.init()
        let vc = MineFaceVC.build(flowDelegate: self)
        let nc = BaseNavController(rootViewController: vc)
        nc.setNavigationBarHidden(true, animated: false)
        let router = Router(rootController: nc)
        self.router = router
    }
    
    func start(with option: MyselfRepresentationInput, animated: Bool) {
        switch option {
        case .window(let window):
            window.rootViewController = router.rootNavController
        }
    }
}

extension MyselfRepresentationFlow: MyselfRepresentationFlowDelegate {
    func nextPressed() {
        let vc = DetailsAboutMeVC.build(flowDelegate: self)
        router.present(vc, transitioningDelegate: transition)
    }
    
    func gotoTestApp() {
        output?(.back)
    }
}
