//
//  TabBarFlow.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import UIKit
import RxSwift

final class TabFlow: BaseCoordinator, Coordinatable {
    
    // MARK: - Coordinatable
    
    typealias InputType = EmptyOption
    typealias OutputType = EmptyOption
    var output: ((EmptyOption) -> Void)?
    
    private let coordinatorFactory: ICoordinatorFactory

    private lazy var forecasFlow: TabIndexCoordinatable = {
        coordinatorFactory.makeForecastCoordinator()
    }()
    
    private lazy var moreAboutMeFlow: TabIndexCoordinatable = {
        coordinatorFactory.makeMoreAboutMeCoordinator()
    }()
    
    init(window: UIWindow,
         coordinatorFactory: ICoordinatorFactory) {
        self.coordinatorFactory = coordinatorFactory
        super.init()
        let tabBarController = UITabBarController()
        tabBarController.setViewControllers(self.makeControllers(), animated: false)

        let nc = BaseNavController(rootViewController: tabBarController)
        nc.setNavigationBarHidden(true, animated: false)
        window.rootViewController = nc
        let router = Router(rootController: nc)
        self.router = router
    }
    
    func start(with option: EmptyOption, animated: Bool) {
    }

    // MARK: - Private
    
    private func makeControllers() -> [UIViewController] {
        guard let forecastVC = forecasFlow.rootViewController,
              let moreAboutMeVC = moreAboutMeFlow.rootViewController else {
                return []
        }
        forecastVC.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
        moreAboutMeVC.tabBarItem = UITabBarItem(tabBarSystemItem: .more, tag: 1)
        let controllers = [forecastVC, moreAboutMeVC]
        
        return controllers
    }
}
