//
//  ICoordinatorFactory.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit

protocol ICoordinatorFactory {
    func makeMyselfRepresentationCoordinator(window: UIWindow) ->
         CoordinatableFactoryResult<MyselfRepresentationInput, DefaultOutput>
    func makeTabCoordinator(window: UIWindow, coordinatorFactory: ICoordinatorFactory) ->
         CoordinatableFactoryResult<EmptyOption, EmptyOption>
    func makeForecastCoordinator() -> TabCoordinatableFactoryResult<EmptyOption, EmptyOption>
    func makeMoreAboutMeCoordinator() -> TabCoordinatableFactoryResult<EmptyOption, EmptyOption>
}
