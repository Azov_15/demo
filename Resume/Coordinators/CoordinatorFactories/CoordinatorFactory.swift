//
//  CoordinatorFactory.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit

final class CoordinatorFactory: ICoordinatorFactory {
    func makeMyselfRepresentationCoordinator(window: UIWindow) ->
         CoordinatableFactoryResult<MyselfRepresentationInput, DefaultOutput> {
        let coordinator = MyselfRepresentationFlow()
        
        return AnyCoordinatable(coordinator)
    }
    
    func makeTabCoordinator(window: UIWindow, coordinatorFactory: ICoordinatorFactory) ->
         CoordinatableFactoryResult<EmptyOption, EmptyOption> {
        let coordinator = TabFlow(window: window, coordinatorFactory: coordinatorFactory)
        
        return AnyCoordinatable(coordinator)
    }
    
    func makeForecastCoordinator() -> TabCoordinatableFactoryResult<EmptyOption, EmptyOption> {
        let coordinator = ForecastFlow(dependencies: ForecastFlowDependencies())
        
        return TabIndexCoordinatable(coordinator)
    }
    
    func makeMoreAboutMeCoordinator() -> TabCoordinatableFactoryResult<EmptyOption, EmptyOption> {
        let coordinator = MoreAboutMeFlow()
        
        return TabIndexCoordinatable(coordinator)
    }
}
