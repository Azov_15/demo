//
//  MoreAboutMeFlow.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import Foundation

final class MoreAboutMeFlow: BaseCoordinator, Coordinatable {
    
    typealias InputType = EmptyOption
    typealias OutputType = EmptyOption
    
    var output: ((EmptyOption) -> Void)?
        
    override init() {
        super.init()
        
        let nc = BaseNavController(rootViewController: MoreAboutMeVC())
        let router = Router(rootController: nc)
        self.router = router
    }
    
    func start(with option: EmptyOption, animated: Bool) {
    }
}
