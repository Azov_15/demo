//
//  ForecastFlowDependencies.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import Foundation

protocol IForecastFlowDependencies {
    var networkService: INetworkService { get }
}

final class ForecastFlowDependencies: IForecastFlowDependencies {
    var networkService: INetworkService
    
    init() {
        networkService = NetworkService()
    }
}
