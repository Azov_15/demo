//
//  ForecastFlow.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import Foundation

protocol ForecastFlowDelegate: AnyObject {
    func showError(withMessage msg: String)
}

final class ForecastFlow: BaseCoordinator, Coordinatable, ForecastFlowDelegate {
    
    typealias InputType = EmptyOption
    typealias OutputType = EmptyOption
    
    var output: ((EmptyOption) -> Void)?
        
    init(dependencies: IForecastFlowDependencies) {
        super.init()
        
        let vc = ForecastVC.build(networkService: dependencies.networkService, flowDelegate: self)
        let nc = BaseNavController(rootViewController: vc)
        let router = Router(rootController: nc)
        self.router = router
    }
    
    func start(with option: EmptyOption, animated: Bool) {
    }
    
    func showError(withMessage msg: String) {
        router.showMessage(withString: msg)
    }
}
