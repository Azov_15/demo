//
//  WeatherNow.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import ObjectMapper

struct WeatherNow: Mappable {
    
    private(set) var temp: Double = 0
    private(set) var conditionText: String = ""
    private(set) var conditionIconUrl: String = ""
    private(set) var wind: Double = 0
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        temp <- map["temp_c"]
        conditionText <- map["condition.text"]
        conditionIconUrl <- map["condition.icon"]
        conditionIconUrl = "http://\(String(conditionIconUrl.dropFirst(2)))"
        wind <- map["wind_kph"]
    }
}
