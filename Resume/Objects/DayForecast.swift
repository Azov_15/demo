//
//  DayForecast.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import ObjectMapper

struct DayForecast: Mappable {
    
    private(set) var date: String = ""
    private(set) var maxTemp: Double = 0
    private(set) var minTemp: Double = 0
    private(set) var conditionText: String = ""
    private(set) var conditionIconUrl: String = ""
    private(set) var maxWind: Double = 0
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        date <- map["date"]
        maxTemp <- map["day.maxtemp_c"]
        minTemp <- map["day.mintemp_c"]
        conditionText <- map["day.condition.text"]
        conditionIconUrl <- map["day.condition.icon"]
        conditionIconUrl = "http://\(String(conditionIconUrl.dropFirst(2)))"
        maxWind <- map["day.maxwind_kph"]
    }
}
