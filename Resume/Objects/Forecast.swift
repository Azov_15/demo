//
//  Forecast.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import ObjectMapper

struct Forecast: Mappable {
    
    private(set) var weatherNow: WeatherNow?
    private(set) var daysForecast: [DayForecast] = []
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        weatherNow <- map["current"]
        daysForecast <- map["forecast.forecastday"]
    }
}
