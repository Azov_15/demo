//
//  DailyForecastCell.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import SnapKit
import SDWebImage

typealias DailyForecastCellConfig = CollectionViewConfigurator<DailyForecastCell, DayForecast>

final class DailyForecastCell: UITableViewCell, ConfigurableCollectionContent {
    
    typealias DataType = DayForecast
    
    private lazy var icon: UIImageView = {
        let imageView = UIImageView()
        
        return imageView
    }()
    
    private lazy var wheatherInfo: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 15)
        label.numberOfLines = 0
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(data: DayForecast) {
        icon.sd_setImage(with: URL(string: data.conditionIconUrl), completed: nil)
        let wheaterInfoText = "\(data.date)\n" +
                              "\(data.conditionText)\n" +
                              "Температура max: \(data.maxTemp), min: \(data.minTemp)\n" +
                              "Ветер до \(data.maxWind) км/ч"
        wheatherInfo.text = wheaterInfoText
    }
    
    func setupUI() {
        contentView.backgroundColor = UIColor.brown.withAlphaComponent(0.5)
        
        contentView.addSubview(icon)
        icon.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(16)
            $0.centerY.equalToSuperview()
            $0.height.width.equalTo(60)
            $0.bottom.equalToSuperview().offset(-16)
        }
        
        contentView.addSubview(wheatherInfo)
        wheatherInfo.snp.makeConstraints {
            $0.leading.equalTo(icon.snp.trailing).offset(16)
            $0.trailing.equalToSuperview().offset(-16)
            $0.centerY.equalToSuperview()
        }
    }
}


