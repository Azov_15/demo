//
//  ForecastHeader.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import SnapKit
import RxSwift

typealias ForecastHeaderConfig = CollectionViewConfigurator<ForecastHeader, ForecastHeaderVM>

struct ForecastHeaderVM {
    var startSearch: PublishSubject<String?> = PublishSubject<String?>()
    var daysChanged: PublishSubject<UInt> = PublishSubject<UInt>()
    var selectedRow: UInt = 0
}

final class ForecastHeader: UITableViewHeaderFooterView, ConfigurableCollectionContent {
    
    typealias DataType = ForecastHeaderVM
    
    lazy var searchTextField: UITextField = {
        let view = UITextField()
        view.text = "Москва"
        view.placeholder = "Введите город"
        view.font = .systemFont(ofSize: 14)
        view.textColor = .black
        view.textAlignment = .left
        view.returnKeyType = .search
        view.borderStyle = .roundedRect
        
        return view
    }()
    
    lazy var daysLabel: UILabel = {
        let label = UILabel()
        label.text = "Дни"
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 12)
        label.textColor = .black
        label.numberOfLines = 0
        
        return label
    }()
    
    private lazy var daysPicker: UIPickerView = {
        let picker = UIPickerView()
        
        return picker
    }()
    
    private var bag = DisposeBag()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
        setupPickerValues()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(data: ForecastHeaderVM) {
        searchTextField
            .rx
            .controlEvent([.editingDidEndOnExit])
            .map({ [weak self] _ -> String? in
                return self?.searchTextField.text
            })
            .bind(to: data.startSearch)
            .disposed(by: bag)
        
        daysPicker
            .rx
            .itemSelected
            .map({ pickerData -> UInt in
                return UInt(pickerData.row)
            })
            .bind(to: data.daysChanged)
            .disposed(by: bag)
        daysPicker.selectRow(Int(data.selectedRow), inComponent: 0, animated: false)
    }
    
    func setupUI() {
        contentView.backgroundColor = .white
        
        contentView.addSubview(searchTextField)
        contentView.addSubview(daysPicker)
        searchTextField.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(16)
            $0.centerY.equalTo(daysPicker.snp.centerY)
            $0.width.equalTo(UIScreen.main.bounds.width * 0.6)
        }
        
        daysPicker.snp.makeConstraints {
            $0.leading.equalTo(searchTextField.snp.trailing)
            $0.bottom.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-16)
        }
        
        contentView.addSubview(daysLabel)
        daysLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8)
            $0.bottom.equalTo(daysPicker.snp.top)
            $0.centerX.equalTo(daysPicker.snp.centerX)
        }
        searchTextField.becomeFirstResponder()
        setupPickerValues()
    }
    
    private func setupPickerValues() {
        Observable.just(["1", "2", "3"])
                        .bind(to: daysPicker.rx.itemTitles) { _, item in
                            return "\(item)"
                        }
                        .disposed(by: bag)
    }
}
