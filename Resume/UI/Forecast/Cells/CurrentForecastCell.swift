//
//  CurrentForecastCell.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import SnapKit
import SDWebImage

typealias CurrentForecastCellConfig = CollectionViewConfigurator<CurrentForecastCell, WeatherNow>

final class CurrentForecastCell: UITableViewCell, ConfigurableCollectionContent {
    
    typealias DataType = WeatherNow
    
    private lazy var icon: UIImageView = {
        let imageView = UIImageView()
        
        return imageView
    }()
    
    private lazy var wheatherInfo: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 15)
        label.numberOfLines = 0
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(data: WeatherNow) {
        icon.sd_setImage(with: URL(string: data.conditionIconUrl), completed: nil)
        let wheaterInfoText = "\(data.conditionText) \(data.temp) градусов \nветер - \(data.wind) км/ч"
        wheatherInfo.text = wheaterInfoText
    }
    
    func setupUI() {
        contentView.backgroundColor = UIColor.green.withAlphaComponent(0.5)
        
        contentView.addSubview(icon)
        icon.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8)
            $0.centerX.equalToSuperview()
            $0.height.width.equalTo(60)
        }
        
        contentView.addSubview(wheatherInfo)
        wheatherInfo.snp.makeConstraints {
            $0.top.equalTo(icon.snp.bottom).offset(8)
            $0.centerX.equalToSuperview()
            $0.leading.equalToSuperview().offset(16)
            $0.trailing.equalToSuperview().offset(-16)
            $0.bottom.equalToSuperview().offset(-8)
        }
    }
}

