//
//  ForecastViewModel.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import RxSwift
import RxCocoa
import ObjectMapper

protocol IForecastViewModel {
    var tapSearch: Binder<String?> { get }
    var day: BehaviorRelay<UInt> { get }
    var forecast: BehaviorRelay<Forecast?> { get }
}

final class ForecastViewModel: IForecastViewModel {
    
    var tapSearch: Binder<String?> {
        return Binder(self) { [weak self] _, city in
            guard let self = self else {
                return
            }
            self.networkService.getForecast(sity: city ?? "", days: self.day.value)
                .subscribe(onNext: {[weak self] json in
                    self?.forecast.accept(Forecast(JSON: json))
                }, onError: { [weak self] error in
                    self?.flowDelegate?.showError(withMessage: error.localizedDescription)
                })
                .disposed(by: self.bag)
        }
    }
    var day: BehaviorRelay<UInt> = BehaviorRelay(value: 0)
    var forecast: BehaviorRelay<Forecast?> = BehaviorRelay(value: nil)
    
    private let networkService: INetworkService
    private weak var flowDelegate: ForecastFlowDelegate?
    private let bag = DisposeBag()
    
    init(networkService: INetworkService, flowDelegate: ForecastFlowDelegate?) {
        self.networkService = networkService
        self.flowDelegate = flowDelegate
    }
}
