//
//  ForecastVCConfig.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import Foundation

extension ForecastVC {
    func createDataSource() -> TableDataSource {
        var dataSource = TableDataSource()
        var vm = ForecastHeaderVM()
        vm.daysChanged.map { $0 + 1 }.bind(to: viewModel.day).disposed(by: bag)
        vm.startSearch.compactMap { $0 }.bind(to: viewModel.tapSearch).disposed(by: bag)
        vm.selectedRow = (viewModel.day.value == 0) ? viewModel.day.value : viewModel.day.value - 1
        let headerConfig = ForecastHeaderConfig(item: vm)
        var section = TableSection(headerViewId: ForecastHeader.reuseIdentifier, headerConfig: headerConfig)
        section.headerHeight = 100
        
        if let forecast = viewModel.forecast.value {
            section.addRow(wheaterNowRow)
            forecast.daysForecast.forEach { dayForecast in
                section.addRow(makeDailyForecastRow(with: dayForecast))
            }
        } else {
            section.addRow(uselessRow)
        }
        
        dataSource.addSection(section)
        
        return dataSource
    }
    
    private var uselessRow: TableRow {
        let uselessVM = TextCellVM(text: "Здесь будет погода",
                                                    textColor: .black,
                                                    textSize: 15,
                                                    alignment: .center)
        let config = TextCellConfig(item: uselessVM)
        let row = TableRow(rowId: TextCell.reuseIdentifier,
                           config: config)
        
        return row
    }
    
    private var wheaterNowRow: TableRow {
        guard let wheaterNow = viewModel.forecast.value?.weatherNow else {
            return TableRow(rowId: "")
        }
        let config = CurrentForecastCellConfig(item: wheaterNow)
        let row = TableRow(rowId: CurrentForecastCell.reuseIdentifier,
                           config: config)
        
        return row
    }
    
    private func makeDailyForecastRow(with forecast: DayForecast) -> TableRow {
        let config = DailyForecastCellConfig(item: forecast)
        let row = TableRow(rowId: DailyForecastCell.reuseIdentifier,
                           config: config)
        return row
    }
}
