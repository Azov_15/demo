//
//  ForecastVC.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import SnapKit
import RxSwift

final class ForecastVC: BaseViewController {
    private lazy var baseTable: TableViewBase = TableViewBase(dataSource: self.createDataSource())
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(cellClass: TextCell.self)
        tableView.register(cellClass: CurrentForecastCell.self)
        tableView.register(cellClass: DailyForecastCell.self)
        tableView.register(ForecastHeader.self,
                           forHeaderFooterViewReuseIdentifier: ForecastHeader.reuseIdentifier)
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.keyboardDismissMode = .interactive
        
        return tableView
    }()
    
    let viewModel: IForecastViewModel
    let bag = DisposeBag()
    
    init(viewModel: IForecastViewModel) {
        self.viewModel = viewModel
        super.init(nibName: .none, bundle: .none)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupBinding()
    }
    
    override func setupUI() {
        super.setupUI()
        baseTable.setupTable(tableView)
        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        }
    }
    
    private func setupBinding() {
        viewModel.forecast
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else {
                    return
                }
                self.baseTable.updateDataSource(self.createDataSource())
                self.tableView.reloadData()
                self.tableView.isScrollEnabled = true
            }).disposed(by: bag)
    }
}

extension ForecastVC {
    class func build(networkService: INetworkService, flowDelegate: ForecastFlowDelegate?) -> UIViewController {
        let vm = ForecastViewModel(networkService: networkService, flowDelegate: flowDelegate)
        let vc = ForecastVC(viewModel: vm)
        
        return vc
    }
}

