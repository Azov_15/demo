//
//  IMineFaceViewModel.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import RxSwift
import RxCocoa

protocol IMineFaceViewModel {
    var tapNext: Binder<Void> { get }
}

final class MineFaceViewModel: IMineFaceViewModel {
    var tapNext: Binder<Void> {
        return Binder(self) { [weak self] _, _ in
            self?.flowDelegate?.nextPressed()
        }
    }
    
    private weak var flowDelegate: MyselfRepresentationFlowDelegate?
    
    init(flowDelegate: MyselfRepresentationFlowDelegate?) {
        self.flowDelegate = flowDelegate
    }
}
