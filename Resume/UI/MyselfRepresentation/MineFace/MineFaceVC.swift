//
//  MineFaceVC.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import SnapKit
import RxSwift

final class MineFaceVC: BaseViewController {
    private lazy var face: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "MyFace")
        imageView.clipsToBounds = true
        imageView.alpha = 0
        
        return imageView
    }()
    
    private lazy var welcomeText: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 25)
        label.text = "Привет! Я Вова."
        label.alpha = 0
        
        return label
    }()
    
    private var nextButton: UIButton = {
        let button = UIButton()
        button.setTitle("Давайте знакомится!", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 20)
        button.alpha = 0
        button.layer.cornerRadius = 6
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.blue.cgColor
        button.setTitleColor(.blue, for: .normal)
        button.contentEdgeInsets = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        
        return button
    }()
    
    private let viewModel: IMineFaceViewModel
    private let bag = DisposeBag()
    
    init(viewModel: IMineFaceViewModel) {
        self.viewModel = viewModel
        super.init(nibName: .none, bundle: .none)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        face.layer.cornerRadius = view.bounds.width*0.5/2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        bindUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimations()
    }
    
    override func setupUI() {
        super.setupUI()
        view.addSubview(face)
        face.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(view.snp.centerY).offset(16)
            $0.width.height.equalTo(view.snp.width).multipliedBy(0.5)
        }
        
        view.addSubview(welcomeText)
        welcomeText.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(view.snp.centerY).offset(32)
        }
        
        view.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.equalTo(welcomeText.snp.bottom).offset(12)
            $0.centerX.equalToSuperview()
        }
    }
        
    func hideAnimation() {
        welcomeText.alpha = 0
        nextButton.alpha = 0
        let scaleFactor: CGFloat = 0.25
        let scaleTransform = CGAffineTransform(scaleX: scaleFactor, y: scaleFactor)
        face.transform = scaleTransform
        face.center = CGPoint(x: 40, y: 48 + view.safeAreaInsets.top/2)
        face.layer.masksToBounds = true
    }
    
    private func bindUI() {
        nextButton.rx
            .tap
            .bind(to: viewModel.tapNext)
            .disposed(by: bag)
    }
    
    private func startAnimations() {
        UIView.animate(withDuration: 1) {
            self.face.alpha = 1
        }
        UIView.animate(withDuration: 1,
                       delay: 0.75,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 5,
                       options: .curveEaseOut,
                       animations: {
                        self.face.snp.remakeConstraints { [unowned self] in
                            $0.centerX.equalToSuperview()
                            $0.bottom.equalTo(view.snp.centerY).offset(16)
                            $0.width.height.equalTo(view.snp.width).multipliedBy(0.5)
                        }
                        self.view.layoutIfNeeded()
                       }, completion: nil)
        UIView.animate(withDuration: 1,
                       delay: 1.75,
                       options: .curveEaseIn,
                       animations: {
                        self.welcomeText.alpha = 1
                       }, completion: nil)
        UIView.animate(withDuration: 1,
                       delay: 2.5,
                       options: .curveLinear,
                       animations: {
                        self.nextButton.alpha = 1
                       }, completion: nil)
    }
}

extension MineFaceVC {
    class func build(flowDelegate: MyselfRepresentationFlowDelegate?) -> UIViewController {
        let vm = MineFaceViewModel(flowDelegate: flowDelegate)
        let vc = MineFaceVC(viewModel: vm)
        
        return vc
    }
}
