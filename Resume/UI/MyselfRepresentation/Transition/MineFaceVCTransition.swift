//
//  MineFaceVCTransition.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit

final class MineFaceVCTransition: NSObject, UIViewControllerTransitioningDelegate {
        
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return MyselfRepresentationAnimator()
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
}
