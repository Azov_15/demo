//
//  MyselfRepresentationAnimator.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit

final class MyselfRepresentationAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration: TimeInterval = 1
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let toVC = transitionContext.viewController(forKey: .to),
            let fromNavController = transitionContext.viewController(forKey: .from) as? UINavigationController,
            let fromVC = fromNavController.viewControllers.first as? MineFaceVC
        else {
            return
        }
        transitionContext.containerView.addSubview(toVC.view)
        toVC.view.alpha = 0
                
        UIView.animate(withDuration: duration, animations: {
            fromVC.hideAnimation()
        }, completion: { _ in
            UIView.animate(withDuration: 0.5) {
                toVC.view.alpha = 1
            } completion: { _ in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }
        })
    }
}
