//
//  DetailAboutMeHeader.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import SnapKit

typealias DetailAboutMeHeaderConfig = CollectionViewConfigurator<DetailAboutMeHeader, DetailAboutMeHeaderVM>

struct DetailAboutMeHeaderVM {
    var text: String
    var image: UIImage?
}

final class DetailAboutMeHeader: UITableViewHeaderFooterView, ConfigurableCollectionContent {
    
    typealias DataType = DetailAboutMeHeaderVM
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 20)
        label.textColor = .black
        label.numberOfLines = 0
        
        return label
    }()
    
    private lazy var face: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 48/2
        
        return imageView
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(data: DetailAboutMeHeaderVM) {
        nameLabel.text = data.text
        face.image = data.image
    }
    
    func setupUI() {
        contentView.backgroundColor = .white
        
        contentView.addSubview(face)
        face.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(16)
            $0.centerY.equalToSuperview()
            $0.width.height.equalTo(48)
        }
        
        contentView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints {
            $0.leading.equalTo(face.snp.trailing).offset(16)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(16)
        }
    }
}
