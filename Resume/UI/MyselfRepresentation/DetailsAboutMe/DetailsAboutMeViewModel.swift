//
//  DetailsAboutMeViewModel.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import RxSwift
import RxCocoa

protocol IDetailsAboutMeViewModel {
    var tapNext: Binder<Void> { get }
    var descriptionText: String { get }
}

final class DetailsAboutMeViewModel: IDetailsAboutMeViewModel {
    var tapNext: Binder<Void> {
        return Binder(self) { [weak self] _, _ in
            self?.flowDelegate?.gotoTestApp()
        }
    }
    
    var descriptionText: String {
        let bundle = Bundle.main
        let path = bundle.path(forResource: "resume_text", ofType: "txt")
        guard let text = try? String(contentsOfFile: path!) else
        {
            return ""
            
        }
        return text
    }
    
    private weak var flowDelegate: MyselfRepresentationFlowDelegate?
    
    init(flowDelegate: MyselfRepresentationFlowDelegate?) {
        self.flowDelegate = flowDelegate
    }
}
