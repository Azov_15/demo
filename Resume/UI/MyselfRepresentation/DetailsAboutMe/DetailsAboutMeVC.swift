//
//  DetailsAboutMeVC.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import SnapKit
import RxSwift

final class DetailsAboutMeVC: BaseViewController {
    private lazy var baseTable: TableViewBase = TableViewBase(dataSource: self.createDataSource())
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(cellClass: TextCell.self)
        tableView.register(DetailAboutMeHeader.self,
                           forHeaderFooterViewReuseIdentifier: DetailAboutMeHeader.reuseIdentifier)
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.contentInsetAdjustmentBehavior = .never
        
        return tableView
    }()
    
    let viewModel: IDetailsAboutMeViewModel
    let bag = DisposeBag()
    
    init(viewModel: IDetailsAboutMeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: .none, bundle: .none)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func setupUI() {
        super.setupUI()
        baseTable.setupTable(tableView)
        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        }
    }
}

extension DetailsAboutMeVC {
    class func build(flowDelegate: MyselfRepresentationFlowDelegate?) -> UIViewController {
        let vm = DetailsAboutMeViewModel(flowDelegate: flowDelegate)
        let vc = DetailsAboutMeVC(viewModel: vm)
        
        return vc
    }
}
