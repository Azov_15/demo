//
//  DetailsAboutMeVCConfig.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit

extension DetailsAboutMeVC {
    func createDataSource() -> TableDataSource {
        var dataSource = TableDataSource()
        let vm = DetailAboutMeHeaderVM(text: "Азов Владимир",
                                       image: UIImage(named: "MyFace"))
        let headerConfig = DetailAboutMeHeaderConfig(item: vm)
        var section = TableSection(headerViewId: DetailAboutMeHeader.reuseIdentifier, headerConfig: headerConfig)
        section.headerHeight = 60
        section.addRow(descriptionRow)
        section.addRow(understandRow)
        
        dataSource.addSection(section)
        return dataSource
    }
    
    private var descriptionRow: TableRow {
        let descriptionVM = TextCellVM(text: viewModel.descriptionText,
                                                    textColor: .black,
                                                    textSize: 15,
                                                    alignment: .left)
        let config = TextCellConfig(item: descriptionVM)
        let row = TableRow(rowId: TextCell.reuseIdentifier,
                           config: config)
        
        return row
    }
    
    private var understandRow: TableRow {
        let descriptionVM = TextCellVM(text: "Понимаю... Приятно познакомится!",
                                                           textColor: .blue,
                                                           textSize: 20,
                                                           alignment: .center)
        let config = TextCellConfig(item: descriptionVM)
        let row = TableRow(rowId: TextCell.reuseIdentifier,
                           config: config)
        row.rx.action.bind(to: viewModel.tapNext).disposed(by: bag)
        
        return row
    }
}
