//
//  MoreAboutMeVCConfig.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import Foundation

extension MoreAboutMeVC {
    func createDataSource() -> TableDataSource {
        var dataSource = TableDataSource()
        var section = TableSection()
        section.addRow(aboutMeRow)
        
        dataSource.addSection(section)
        
        return dataSource
    }
    
    private var aboutMeRow: TableRow {
        let aboutMeVM = TextCellVM(text: descriptionText,
                                   textColor: .black,
                                   textSize: 15,
                                   alignment: .left)
        let config = TextCellConfig(item: aboutMeVM)
        let row = TableRow(rowId: TextCell.reuseIdentifier,
                           config: config)
        
        return row
    }
    
    private var descriptionText: String {
        let bundle = Bundle.main
        let path = bundle.path(forResource: "about_me", ofType: "txt")
        guard let text = try? String(contentsOfFile: path!) else
        {
            return ""
            
        }
        return text
    }
}
