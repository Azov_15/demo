//
//  MoreAboutMeVC.swift
//  Resume
//
//  Created by Azov Vladimir on 27.05.2021.
//

import SnapKit
import RxSwift

final class MoreAboutMeVC: BaseViewController {
    private lazy var baseTable: TableViewBase = TableViewBase(dataSource: self.createDataSource())
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(cellClass: TextCell.self)
        tableView.register(ForecastHeader.self,
                           forHeaderFooterViewReuseIdentifier: ForecastHeader.reuseIdentifier)
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.contentInsetAdjustmentBehavior = .never
        
        return tableView
    }()
    
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func setupUI() {
        super.setupUI()
        baseTable.setupTable(tableView)
        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        }
    }
}

extension MoreAboutMeVC {
    class func build() -> UIViewController {
        let vc = MoreAboutMeVC()
        
        return vc
    }
}
