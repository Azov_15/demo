//
//  TextCell.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit

typealias TextCellConfig = CollectionViewConfigurator<TextCell, TextCellVM>

struct TextCellVM {
    var text: String
    var textColor: UIColor
    var textSize: CGFloat
    var alignment: NSTextAlignment
}

final class TextCell: UITableViewCell, ConfigurableCollectionContent {
    
    typealias DataType = TextCellVM
    
    lazy var textDescription: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(data: TextCellVM) {
        textDescription.text = data.text
        textDescription.textColor = data.textColor
        textDescription.font = .systemFont(ofSize: data.textSize)
        textDescription.textAlignment = data.alignment
    }
    
    func setupUI() {
        contentView.backgroundColor = .white
        
        contentView.addSubview(textDescription)
        textDescription.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8)
            $0.leading.equalToSuperview().offset(16)
            $0.trailing.equalToSuperview().offset(-16)
            $0.bottom.equalToSuperview().offset(-8)
        }
    }
}
