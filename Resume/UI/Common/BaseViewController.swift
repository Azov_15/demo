//
//  BaseViewController.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit

class BaseViewController: UIViewController {
    func setupUI() {
        view.backgroundColor = .white
    }
}
