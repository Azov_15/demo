//
//  Coordinatable.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import Foundation
import UIKit

protocol Coordinatable: AnyObject {
    associatedtype InputType
    associatedtype OutputType

    func start(with option: InputType, animated: Bool)
    var output: ((OutputType) -> Void)? { get set }
}

// swiftlint:disable superfluous_disable_command
typealias CoordinatableFactoryResult<Input, Output> = (AnyCoordinatable<Input, Output>)

//swiftlint:disable final_class
class AnyCoordinatable<InputType, OutputType>: Coordinatable {
    private let setOutputClosure: (((OutputType) -> Void)?) -> Void
    private let getOutputClosure: () -> ((OutputType) -> Void)?
    private let startClosure: (InputType, Bool) -> Void

    let coordinator: Any?

    init<T: Coordinatable>(_ coordinator: T) where T.InputType == InputType, T.OutputType == OutputType {
        self.coordinator = coordinator
        startClosure = { (option, animated) in
            coordinator.start(with: option, animated: animated)
        }

        getOutputClosure = { () -> ((OutputType) -> Void)? in
            return coordinator.output
        }

        setOutputClosure = { output in
            coordinator.output = output
        }
    }

    func start(with option: InputType, animated: Bool) {
        startClosure(option, animated)
    }

    var output: ((OutputType) -> Void)? {
        get {
            return getOutputClosure()
        }

        set {
            setOutputClosure(newValue)
        }
    }
}

typealias TabCoordinatableFactoryResult<Input, Output> = (TabIndexCoordinatable<Input, Output>)

final class TabIndexCoordinatable<InputType, OutputType>: AnyCoordinatable<InputType, OutputType> {
    
    var rootViewController: UIViewController?
    
    override init<T: Coordinatable>(_ coordionator: T) where InputType == T.InputType, OutputType == T.OutputType {
        super.init(coordionator)
        
        if let baseCoordinator = coordionator as? BaseCoordinator {
            rootViewController = baseCoordinator.router.rootNavController
        }
    }
}
