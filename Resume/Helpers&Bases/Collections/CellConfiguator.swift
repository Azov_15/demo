//
//  CellConfiguator.swift
//  aitube
//
//  Created by Azov Vladimir on 02.06.2020.
//  Copyright © 2020 aitu-dala. All rights reserved.
//

import UIKit

protocol ConfigurableCollectionContent {
    associatedtype DataType
    func configure(data: DataType)
}

protocol ContentConfigurator {
    func configure(view: UIView)
}

final class CollectionViewConfigurator<ContentType: ConfigurableCollectionContent, DataType>: ContentConfigurator
where ContentType.DataType == DataType, ContentType: UIView {
        
    private let item: DataType
    
    init(item: DataType) {
        self.item = item
    }
    
    func configure(view: UIView) {
        if let view = view as? ContentType {
            view.configure(data: item)
        }
    }
}
