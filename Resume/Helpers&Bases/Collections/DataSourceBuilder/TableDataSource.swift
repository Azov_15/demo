//
//  TableDataSource.swift
//  BubbleComics
//
//  Created by Azov Vladimir on 15/03/2019.
//  Copyright © 2019 Bubble. All rights reserved.
//

import UIKit
import RxSwift

private struct EmptyContentConfig: ContentConfigurator {
    func configure(view: UIView) {
    }
}

final class TableRow {
    var rowId: String
    var config: ContentConfigurator
    var height: CGFloat = UITableView.automaticDimension
    var action = {}
    var deselectAction = {}
    var willDisplay: (UITableViewCell) -> Void = { _ in }
    
    init(rowId: String,
         config: ContentConfigurator = EmptyContentConfig(),
         height: CGFloat = UITableView.automaticDimension) {
        self.rowId = rowId
        self.config = config
        self.height = height
    }
}

struct TableSection {
    var headerViewId: String?
    var headerHeight: CGFloat = 0
    var headerConfig: ContentConfigurator
    var rows: [TableRow] = []

    mutating func addRow(_ row: TableRow) {
        rows.append(row)
    }

    mutating func addRows(_ rows: [TableRow]) {
        self.rows.append(contentsOf: rows)
    }

    init(headerViewId: String? = nil,
         headerConfig: ContentConfigurator = EmptyContentConfig()) {
        self.headerViewId = headerViewId
        self.headerConfig = headerConfig
    }
    
    func config(header: UITableViewHeaderFooterView) {
        headerConfig.configure(view: header)
    }
}

struct TableDataSource {
    var sections: [TableSection] = []
    
    var lastIndexPath: IndexPath {
        let lastSection = sections.count - 1
        let lastRow = sections[lastSection].rows.count - 1
        return IndexPath(row: lastRow, section: lastSection)
    }

    mutating func addSection(_ section: TableSection) {
        sections.append(section)
    }

    mutating func addSections(_ sections: [TableSection]) {
        self.sections.append(contentsOf: sections)
    }

    func rowFromIndexPath(_ indexPath: IndexPath) -> TableRow {
        return sections[indexPath.section].rows[indexPath.row]
    }
}
