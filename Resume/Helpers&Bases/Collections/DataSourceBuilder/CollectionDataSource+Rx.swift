//
//  CollectionDataSource+Rx.swift
//  aitube
//
//  Created by Azov Vladimir on 30.04.2021.
//  Copyright © 2021 aitu-dala. All rights reserved.
//

import RxSwift
import RxCocoa

extension CollectionRow: ReactiveCompatible {}

extension Reactive where Base: CollectionRow {
    
    var action: Observable<Void> {
        return Observable.create { observer in
            base.action = {
                observer.onNext(())
            }
            
            return Disposables.create()
        }
    }
}
