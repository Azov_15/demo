//
//  TableDataSource+Rx.swift
//  aitube
//
//  Created by Azov Vladimir on 28.01.2021.
//  Copyright © 2021 aitu-dala. All rights reserved.
//

import RxSwift
import RxCocoa

extension TableRow: ReactiveCompatible {}

extension Reactive where Base: TableRow {
    
    var action: Observable<Void> {
        return Observable.create { observer in
            base.action = {
                observer.onNext(())
            }
            
            return Disposables.create()
        }
    }
}
