//
//  IRouter.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit

protocol IRouter: AnyObject {
    var rootNavController: UINavigationController? { get }
    
    func present(_ viewController: UIViewController?)
    func present(_ viewController: UIViewController?, animated: Bool)
    func present(_ viewController: UIViewController?, animated: Bool, completion: (() -> Void)?)
    func dismissViewController()
    func present(_ viewController: UIViewController?, transitioningDelegate: UIViewControllerTransitioningDelegate?)
    func dismissViewController(animated: Bool, completion: (() -> Void)?)
    
    func setRootNavController(_ navigationController: UINavigationController?)
    func setRootViewController(_ viewController: UIViewController?)
    
    func push(_ viewController: UIViewController?)
    func push(_ viewController: UIViewController?, animated: Bool)
    func push(_ viewController: UIViewController?, hideBottomBar: Bool)
    
    func popViewController()
    func popViewController(animated: Bool)
    func popViewControllers(number: Int, animated: Bool)
    func popToRootViewController(animated: Bool)
    
    func showMessage(withString msg: String)
}

