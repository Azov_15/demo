//
//  Router.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit

final class Router: IRouter {
        
    weak var rootNavController: UINavigationController?
    
    init(rootController: UINavigationController = UINavigationController()) {
        self.rootNavController = rootController
    }
    
    func setRootNavController(_ navigationController: UINavigationController?) {
        self.rootNavController = navigationController
    }
    
    func present(_ viewController: UIViewController?) {
        present(viewController, animated: true)
    }
    
    func present(_ viewController: UIViewController?,
                 transitioningDelegate: UIViewControllerTransitioningDelegate?) {
        if transitioningDelegate != nil {
            viewController?.modalPresentationStyle = .custom
            viewController?.transitioningDelegate = transitioningDelegate
        }
        present(viewController)
    }

    
    func present(_ viewController: UIViewController?, animated: Bool) {
        guard let viewController = viewController else {
            return
        }
        present(viewController, animated: animated, completion: nil)
    }
    
    func present(_ viewController: UIViewController?, animated: Bool, completion: (() -> Void)? = nil) {
        guard let viewController = viewController else {
            return
        }
        rootNavController?.present(viewController, animated: animated, completion: completion)
    }
    
    func dismissViewController() {
        dismissViewController(animated: true, completion: nil)
    }
    
    func dismissViewController(animated: Bool, completion: (() -> Void)?) {
        // Due to iOS 12 stupid behaviour
        if #available(iOS 13.0, *) {
            rootNavController?.dismiss(animated: animated, completion: completion)
        } else {
            completion?()
        }
    }
    
    func push(_ viewController: UIViewController?) {
        push(viewController, animated: true)
    }
    
    func push(_ viewController: UIViewController?, hideBottomBar: Bool) {
        viewController?.hidesBottomBarWhenPushed = hideBottomBar
        push(viewController, animated: true)
    }
    
    func push(_ viewController: UIViewController?, animated: Bool) {
        guard
            let viewController = viewController,
            (viewController is UINavigationController == false)
            else {
                assertionFailure("Deprecated push UINavigationController.")
                return
        }
        rootNavController?.pushViewController(viewController, animated: animated)
    }
    
    func popViewController() {
        popViewController(animated: true)
    }
    
    func popViewController(animated: Bool) {
        rootNavController?.popViewController(animated: animated)
    }

    func popViewControllers(number: Int, animated: Bool) {
        let popNumber = number + 1
        guard let viewControllers = rootNavController?.viewControllers,
            viewControllers.count >= popNumber else {
                return
        }
        rootNavController?.popToViewController(viewControllers[viewControllers.count - popNumber], animated: animated)
    }
    
    func setRootViewController(_ viewController: UIViewController?) {
        guard let viewController = viewController else {
            return
        }
        rootNavController?.setViewControllers([viewController], animated: false)
    }
    
    func popToRootViewController(animated: Bool) {
        rootNavController?.popToRootViewController(animated: animated)
    }

    func showMessage(withString msg: String) {
        let alert = UIAlertController(title: "Ошибка", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: nil))
        rootNavController?.present(alert, animated: true, completion: nil)
    }
}
