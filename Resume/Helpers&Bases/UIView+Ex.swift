//
//  UIView+Ex.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit

extension UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableView {
    func register<CellClass: UITableViewCell>(cellClass: CellClass.Type) {
        register(cellClass, forCellReuseIdentifier: cellClass.reuseIdentifier)
    }
}
