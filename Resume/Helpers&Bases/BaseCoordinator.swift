//
//  BaseCoordinator.swift
//  Resume
//
//  Created by Azov Vladimir on 26.05.2021.
//

import UIKit
import RxSwift
import RxCocoa

class BaseCoordinator: NSObject {
    
    var childCoordinators: [AnyObject] = []
    var router: IRouter
    var bag: DisposeBag = DisposeBag()
    
    init(router: IRouter) {
        self.router = router
        super.init()
    }
    
    override init() {
        router = Router()
        super.init()
    }
    
    func addDependency(_ coordinator: AnyObject) {
        guard !childCoordinators.contains(where: { $0 === coordinator }) else {
            return
        }
        childCoordinators.append(coordinator)
    }
    
    func removeDependency(_ coordinator: AnyObject) {
        guard childCoordinators.isEmpty == false else {
            return
        }
        
        // Clear child-coordinators recursively
        if let coordinator = coordinator as? BaseCoordinator, !coordinator.childCoordinators.isEmpty {
            coordinator.childCoordinators
                .filter({ $0 !== coordinator })
                .forEach({ coordinator.removeDependency($0) })
        }
        
        for (index, element) in childCoordinators.enumerated() where element === coordinator {
            childCoordinators.remove(at: index)
            break
        }
    }
    
    func removeAllDependencies() {
        childCoordinators.forEach { coordinator in
            removeDependency(coordinator)
        }
    }
}
